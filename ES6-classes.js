// თვქენი დავალებაა გააკეთოთ character  ების კლასი სადაც შეინახავთ შემდეგ ინფორმაციას: სახელი და როლი
// (როლი შეიძლება იყოს რამოდენიმენაირი: mage, support, assassin, adc, tank (არ არის აუცილებელი ამ ჩამონათვლის სადმე შენახვა
//   ან ყველა როლის გამოყენება)პ0ო, აქედან support mage  - ც არის. ყველა პერსონაჟს სახელთან და როლთან ერთად აქვს armor ისა და
//   damage ის მეთოდები, რომლებიც უბრალოდ ლოგავენ თუ რამდენი არმორი აქვს და რამდენი dmg აქვს პერსონაჟს. damage ის შემთხვევაში
//   შეგვიძლია გვააკეთოთ შემოწმება mage, support ზე და სხვა როლებზე, პირველი ორის შემთხვევაში პერსონაჟი magic damage ს აკეთებს,
//   ხოლო სხვა დანარჩენ შემთვევებში attack damage და მეთოდიც ამას ლოგავს.  mage ის კლასი ასევე მიიღებს magicPowers და აქვს ფრენის
//   მეთოდიც (mage -ები დაფრინავენ კიდეც), რომელიც დალოგავს, რომ ამ მეიჯს შეუძლია ფრენაც. support იგივე მეიჯია, რომელიც მიიღებს
//   heals - ს (თუ რამდენი "ერთეულის" დაჰილვა შეუძლია) ცვლადად და ექნნება  healing მეთოდი რომელიც დალოგავს, რომ ამ გმირს
//   შეუძლია გარკვეული რაოდენობის ერთეულის დაჰილვა. ასევე გვყავს adc რომელიც მიიღებს attackdamages ს და აქვს მეთოდი, რომელიც
//   ითვლის მის რეინჯს (მეთოდმა რეინჯის მნიშვნელობა უნდა მიიღოს), ამაზე დაყრდნობით ის ლოგავს ახლო რეინჯიანია პერსონაჟი თუ არა.
//   (პირობითად ვთქვათ, რომ 30 < ახლო რეინჯში ითვლება)

//  რაც შეეხება თვითონ პერსონაჟების სახელებს და ყველა იმ მნიშვნელობებს რომლებიც კლასებს უნდა გადასცეთ its up to you. თქვენი ამოცანა
//  კლასების შექმნაში და მათი სტრუქტურის სწორ აგებულებაში მდგომარეობს.

class Character {
  constructor(name, role, armorNumber, damageNumber) {
    this.name = name;
    this.role = role;
    this.armorNumber = armorNumber;
    this.damageNumber = damageNumber;
  }

  armor() {
    console.log(`${this.name} has ${this.armorNumber} armor`);
    return this;
  }

  damage() {
    console.log(`${this.name} has ${this.damageNumber} attack damages`);
    return this;
  }
}

class Mage extends Character {
  constructor(name, armorNumber, damageNumber) {
    super(name, "Mage", armorNumber, damageNumber);
  }
  damage() {
    console.log(`${this.name} has ${this.damageNumber} magic damages`);
    return this;
  }
  magicPowers() {
    console.log(`${this.name} has a magic power, it flies`);
    return this;
  }
}

class Support extends Mage {
  constructor(name, armorNumber, damageNumber) {
    super(name, "Support", armorNumber, damageNumber);
  }
  heals = [];
  heal(number) {
    this.heals.push(number);
    console.log(`${this.name} can heal ${this.heals} `);
  }
}

class Adc extends Character {
  ranges = [];
  range(number) {
    this.ranges.push(number);
    number < 30
      ? console.log(`${this.name} is in close range `)
      : console.log(`${this.name} is in far range `);
  }
  constructor(name, armorNumber, damageNumber) {
    super(name, "Adc", armorNumber, damageNumber);
  }
}
//Character
let Elene = new Character("Elene", "assassin", 23, 30);
console.log(Elene);
Elene.armor();
Elene.damage();
//Mage
let Mariam = new Mage("Mariam", 25, 27);
console.log(Mariam);
Mariam.armor();
Mariam.damage();
Mariam.magicPowers();
//Support
let Giorgi = new Support("Giorgi", 25, 20, 40);
console.log(Giorgi);
Giorgi.armor();
Giorgi.damage();
Giorgi.heal(30);
//Adc
let Saba = new Adc("Saba", 28, 27);
console.log(Saba);
Saba.armor();
Saba.damage();
Saba.range(25);
