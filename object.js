const krediti = [];
krediti[0] = 4;
krediti[1] = 7;
krediti[2] = 6;
krediti[3] = 3;
const sulKreditebi = krediti[0] + krediti[1] + krediti[2] + krediti[3];

let students = [];

students[0] = {
  name: "Jan",
  lastName: "Reno",
  age: 26,
  scores: {
    frontEnd: {
      javascript: 62,
      react: 57,
    },
    backEnd: {
      python: 88,
      java: 90,
    },
  },
};

students[0].metrics = {};
students[0].metrics.sumScore =
  students[0].scores.frontEnd.javascript +
  students[0].scores.frontEnd.react +
  students[0].scores.backEnd.python +
  students[0].scores.backEnd.java;
students[0].metrics.averageScore = students[0].metrics.sumScore / 4;
students[0].metrics.averageFront =
  (students[0].scores.frontEnd.javascript + students[0].scores.frontEnd.react) /
  2;
students[0].metrics.gpa =
  (1 * krediti[0] + 0.5 * krediti[1] + 3 * krediti[2] + 3 * krediti[3]) /
  sulKreditebi;

students[1] = {
  name: "Klod",
  lastName: "Mone",
  age: 19,
  scores: {
    frontEnd: {
      javascript: 77,
      react: 52,
    },
    backEnd: {
      python: 92,
      java: 67,
    },
  },
};
students[1].metrics = {};
students[1].metrics.sumScore =
  students[1].scores.frontEnd.javascript +
  students[1].scores.frontEnd.react +
  students[1].scores.backEnd.python +
  students[1].scores.backEnd.java;
students[1].metrics.averageScore = students[1].metrics.sumScore / 4;
students[1].metrics.averageFront =
  (students[1].scores.frontEnd.javascript + students[1].scores.frontEnd.react) /
  2;
students[1].metrics.gpa =
  (2 * krediti[0] + 0.5 * krediti[1] + 4 * krediti[2] + 1 * krediti[3]) /
  sulKreditebi;

students[2] = {
  name: "Van",
  lastName: "Gogi",
  age: 21,
  scores: {
    frontEnd: {
      javascript: 51,
      react: 98,
    },
    backEnd: {
      python: 65,
      java: 70,
    },
  },
};
students[2].metrics = {};
students[2].metrics.sumScore =
  students[2].scores.frontEnd.javascript +
  students[2].scores.frontEnd.react +
  students[2].scores.backEnd.python +
  students[2].scores.backEnd.java;
students[2].metrics.averageScore = students[2].metrics.sumScore / 4;
students[2].metrics.averageFront =
  (students[2].scores.frontEnd.javascript + students[2].scores.frontEnd.react) /
  2;
students[2].metrics.gpa =
  (0.5 * krediti[0] + 4 * krediti[1] + 1 * krediti[2] + 1 * krediti[3]) /
  sulKreditebi;

students[3] = {
  name: "Dam",
  lastName: "Sqveri",
  age: 36,
  scores: {
    frontEnd: {
      javascript: 82,
      react: 53,
    },
    backEnd: {
      python: 80,
      java: 65,
    },
  },
};
students[3].metrics = {};
students[3].metrics.sumScore =
  students[3].scores.frontEnd.javascript +
  students[3].scores.frontEnd.react +
  students[3].scores.backEnd.python +
  students[3].scores.backEnd.java;
students[3].metrics.averageScore = students[3].metrics.sumScore / 4;
students[3].metrics.averageFront =
  (students[3].scores.frontEnd.javascript + students[3].scores.frontEnd.react) /
  2;
students[3].metrics.gpa =
  (3 * krediti[0] + 0.5 * krediti[1] + 2 * krediti[2] + 1 * krediti[3]) /
  sulKreditebi;

console.log(students);

//გამოვთვალოთ საშუალო ყველასთვის ერთად

let averageAll =
  (students[0].metrics.sumScore +
    students[1].metrics.sumScore +
    students[2].metrics.sumScore +
    students[3].metrics.sumScore) /
  16;

console.log(averageAll);

// სტუდენტებს, რომელთა ცალკე საშუალო არითმეტიკული მეტი აქვთ ვიდრე საერთო არითმეტიკული, მივანიჭოთ "წითელი დიპლომის მქონეს"
//  სტატუსი, ხოლო ვისაც საერთო საშუალოზე ნაკლები აქვს - "ვრაგ ნაროდა" სტატუსი

students[0].metrics.averageScore > averageAll
  ? console.log(students[0].name + " has Tsiteli Diplomi")
  : console.log(students[0].name + " Vrag naroda");
students[1].metrics.averageScore > averageAll
  ? console.log(students[1].name + " has Tsiteli Diplomi")
  : console.log(students[1].name + " Vrag naroda");

students[2].metrics.averageScore > averageAll
  ? console.log(students[2].name + " has Tsiteli Diplomi")
  : console.log(students[2].name + " Vrag naroda");

students[3].metrics.averageScore > averageAll
  ? console.log(students[3].name + " has Tsiteli Diplomi3")
  : console.log(students[3].name + " Vrag naroda");

//გამოვავლინოთ საუკეთესო სტუდენტი GPA-ის მიხედვით

students[0].metrics.gpa > students[1].metrics.gpa &&
students[0].metrics.gpa > students[2].metrics.gpa &&
students[0].metrics.gpa > students[3].metrics.gpa
  ? console.log("Jan has best GPA ever")
  : students[1].metrics.gpa > students[2].metrics.gpa &&
    students[1].metrics.gpa > students[3].metrics.gpa
  ? console.log("Klod has best GPA ever")
  : students[2].metrics.gpa > students[3].metrics.gpa
  ? console.log("Van Gog has best GPA ever")
  : console.log("Dam has best GPA ever");

// -------------------IF ის გამოყენებით------------------
//ყველა შემთხვევა, როცა ჟანის  GPA ყველაზე მაღალია
// if (
//   students[0].metrics.gpa > students[1].metrics.gpa &&
//   students[0].metrics.gpa > students[2].metrics.gpa &&
//   students[0].metrics.gpa > students[3].metrics.gpa
// )
//   console.log("Jan has best GPA ever");
// //ყველა შემთხვევა, როცა კლოდ მონეს GPA ყველაზე მაღალია
// else if (
//   students[1].metrics.gpa > students[2].metrics.gpa &&
//   students[1].metrics.gpa > students[3].metrics.gpa
// )
//   console.log("Klod has best GPA ever");
// //ყველა შემთხვევა, როცა ვან გოგის GPA ყველაზე მაღალია
// else if (students[2].metrics.gpa > students[3].metrics.gpa)
//   console.log("Van Gog has best GPA ever");
// //დარჩენილი შემთხვევა, როცა დამ სქვეარის GPA ყველაზე მაღალია
// else console.log("Dam has best GPA ever");

//გამოვავლინეთ საუკეთესო სტუდენტი საშუალო ქულების მიხედვით 21+ ასაკში

if (
  students[0].metrics.averageScore > students[1].metrics.averageScore &&
  students[0].age > 21 &&
  students[0].metrics.averageScore > students[2].metrics.averageScore &&
  students[0].metrics.averageScore > students[3].metrics.averageScore
)
  console.log("Jan is over 21 year and has the best average scores ");
//ყველა შემთხვევა, როცა კლოდ მონეს საშუალო ქულა ყველაზე მაღალია
else if (
  students[1].metrics.averageScore > students[2].metrics.averageScore &&
  students[0].age > 21 &&
  students[1].metrics.averageScore > students[3].metrics.averageScore
)
  console.log("Klod is over 21 year and has the best average scores");
//ყველა შემთხვევა, როცა ვან გოგის საშუალო ქულა ყველაზე მაღალია
else if (
  students[2].metrics.averageScore > students[3].metrics.averageScore &&
  students[2].age > 21
)
  console.log("Van Gog is over 21 year and has the best average scores");
//დარჩენილი შემთხვევა, როცა დამ სქვეარის საშუალო ქულა ყველაზე მაღალია
else console.log("Dam is over 21 year and has the best average scores");

// გამოავლინოთ სტუდენტი რომელიც საუკეთესოა ფრონტ-ენდის საგნებში საშუალო ქულების მიხედვით (js, react)

if (
  students[0].metrics.averageFront > students[1].metrics.averageFront &&
  students[0].metrics.averageFront > students[1].metrics.averageFront &&
  students[0].metrics.averageFront > students[3].metrics.averageFront
)
  console.log("Jan  has the best average front-end scores ");
//ყველა შემთხვევა, როცა კლოდ მონეს ფრონტ-ენდის საგნებში საშუალო ყველაზე მაღალია
else if (
  students[1].metrics.averageFront > students[2].metrics.averageFront &&
  students[1].metrics.averageFront > students[3].metrics.averageFront
)
  console.log("Klod has the best average front-end scores");
//ყველა შემთხვევა, როცა ვან გოგის ფრონტ-ენდის საგნებში საშუალო ყველაზე მაღალია
else if (students[2].metrics.averageFront > students[3].metrics.averageFront)
  console.log("Van Gog has the best average front-end scores");
//დარჩენილი შემთხვევა, როცა დამ სქვეარის ფრონტ-ენდის საგნებში საშუალო ყველაზე მაღალია
else console.log("Dam has the best average front-end scores");

// ----------------------------არასწორად გამომივიდა, როგორც კი გამოავლინა, აღარ გააგრძელა-----------------------------------------------

// let highestFrontAverage = students[0].metrics.averageFront;

// highestFrontAverage =
//   students[1].metrics.averageFront > highestFrontAverage
//     ? students[1]
//     : highestFrontAverage;
// highestFrontAverage =
//   students[2].metrics.averageFront > highestFrontAverage
//     ? students[2]
//     : highestFrontAverage;
// highestFrontAverage =
//   students[3].metrics.averageFront > highestFrontAverage
//     ? students[3]
//     : highestFrontAverage;

// console.log(`${highestFrontAverage["name"]} has max front points`);
