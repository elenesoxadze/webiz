let tourists = [
  {
    name: "Mark",
    age: 19,
    visitedCities: ["Tbilisi", "Londoni", "Romi", "Berlini"],
    expanses: [120, 200, 150, 140],
  },
  {
    name: "Bob",
    age: 21,
    visitedCities: ["Maiami", "Moskovi", "Vena", "Riga", " Kievi"],
    expanses: [90, 240, 100, 76, 123],
  },
  {
    name: "Sam",
    age: 22,
    visitedCities: ["Tbilisi", "Budapeshti", "Varshava", " Vilniusi"],
    expanses: [118, 95, 210, 236],
  },

  {
    name: "Anna",
    age: 20,
    visitedCities: ["newYork", "Ateni", "Sidnei", " Tokio"],
    expanses: [100, 240, 50, 190],
  },

  {
    name: "Alex",
    age: 23,
    visitedCities: ["Parisi", "Tbilisi", "Madridi", " Marseli", "Minski"],
    expanses: [96, 134, 76, 210, 158],
  },
];

// არის თუ არა ის ზრდასრული (21+, ზრდასრულში 21 წელიც იგუისხმება), პასუხის მიხედვით შექმენით შესაბამისი key და შეინახეთ
// შესაბამისი სტუდენტის ინფოში როგორც boolean ტიპის ცვლადი.

for (let i = 0; i < tourists.length; i++) {
  tourists[i].adult = tourists[i].age >= 21;

  //გაარკვიეთ თითოეული სტუდენტისთვის არის თუ არა ის ნამყოფი საქართველოში და პასუხის მიხედვით წინა დავალების ანალოგიურად
  // შექმენით შესაბამისი key და შეინახეთ შესაბამისი სტუდენტის ინფოში ამჯერად რაიმე სტინგის სახით (ნამყოფია, არაა ნამყოფი და ა.შ.).

  for (let j = 0; j < tourists[i].visitedCities.length; j++) {
    tourists[i].visitedCities[j] === "Tbilisi"
      ? (tourists[i].status = "namyofia")
      : (tourists[i].status = "araa namyofia");

    if (tourists[i].status === "namyofia") {
      break;
    }
  }

  // დაითვალეთ თითოეული ტურისტისთვის ჯამში რა თანხა დახარჯა მოგზაურობისას.

  tourists[i].totalExpanses = 0;
  for (let m = 0; m < tourists[i].expanses.length; m++) {
    tourists[i].totalExpanses += tourists[i].expanses[m];
  }

  // დაითვალეთ თითოეული ტურისტისთვის საშუალოდ რა თანხა დახარჯა მოგზაურობისას.
  tourists[i].averageExpanses =
    tourists[i].totalExpanses / tourists[i].expanses.length;
}

//  გამოავლინეთ ყველაზე "მხარჯველი" ტურისტი, დალოგეთ ვინ დახარჯა ყველაზე მეტი და რამდენი.
let mkharjveli = tourists[0].totalExpanses;
if (
  tourists[0].totalExpanses > tourists[1].totalExpanses &&
  tourists[0].totalExpanses > tourists[2].totalExpanses &&
  tourists[0].totalExpanses > tourists[3].totalExpanses &&
  tourists[0].totalExpanses > tourists[4].totalExpanses
)
  console.log(`${tourists[0].name}  aris mkharjveli `);
else if (
  tourists[1].totalExpanses > tourists[2].totalExpanses &&
  tourists[1].totalExpanses > tourists[3].totalExpanses &&
  tourists[1].totalExpanses > tourists[4].totalExpanses
)
  console.log(`${tourists[1].name} aris mkharjveli`);
else if (
  tourists[2].totalExpanses > tourists[3].totalExpanses &&
  tourists[2].totalExpanses > tourists[4].totalExpanses
)
  console.log(`${tourists[2].name} aris mkharjveli`);
else if (tourists[3].totalExpanses > tourists[4].totalExpanses)
  console.log(`${tourists[3].name} aris mkharjveli`);
else console.log(`${tourists[4].name} aris mkharjveli`);

console.log(tourists);
