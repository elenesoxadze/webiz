let sumAll = 0;
let studentsLength = 0;
const krediti = [];
krediti[0] = 4;
krediti[1] = 7;
krediti[2] = 6;
krediti[3] = 3;
const sulKreditebi = krediti[0] + krediti[1] + krediti[2] + krediti[3];

students = [
  {
    name: "Jan",
    lastName: "Reno",
    age: 26,
    scoresFront: {
      javascript: 62,
      react: 57,
    },
    scoresAll: [62, 57, 88, 90],
    metrics: { sum: {}, averageFront: {}, average: {} },
  },

  {
    name: "Klod",
    lastName: "Mone",
    age: 19,
    scoresFront: {
      javascript: 77,
      react: 52,
    },
    scoresAll: [77, 52, 92, 67],
    metrics: { sum: {}, averageFront: {}, average: {} },
  },

  {
    name: "Van",
    lastName: "Gogi",
    age: 21,
    scoresFront: {
      javascript: 51,
      react: 98,
    },
    scoresAll: [51, 98, 65, 70],
    metrics: { sum: {}, averageFront: {}, average: {} },
  },
  {
    name: "Dam",
    lastName: "Sqveri",
    age: 36,
    scoresFront: {
      javascript: 82,
      react: 53,
    },
    scoresAll: [82, 53, 80, 65],
    metrics: { sum: {}, averageFront: {}, average: {} },
  },
];

// გამოვთვალოთ ქულების ჯამი თითოეულისთვის

for (let i = 0; i < students.length; i++) {
  students[i].metrics.sum = 0;
  for (let j = 0; j < students[i].scoresAll.length; j++) {
    students[i].metrics.sum += students[i].scoresAll[j];
  }
  // გამოვთვალოთ ქულების საშუალო ყველასთვის

  sumAll += students[i].metrics.sum;

  averageAll = sumAll / (students.length * 4);

  // გამოვთვალოთ საშუალო თითოეული სტუდენტისთვის frontEnd-ში

  students[i].metrics.averageFront =
    (students[i].scoresFront.javascript + students[i].scoresFront.react) / 2;

  // გამოვთვალოთ საშუალო თითოეული სტუდენტისთვის

  students[i].metrics.average = students[i].metrics.sum / 4;
}
console.log(averageAll);
console.log(students);
