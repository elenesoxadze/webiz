// დავალებად გექნებათ ორი ამოცანის ამოხსნა:
// 1. Check Winner Team

// ორი გუნდი ეჯიბრება ერთმანეთს კერლინგში, გუნდი PowerRangers და გუნდი FairyTails. თითოეული გუნდი ეჯიბრება ერთმანეთს 3-3 ჯერ, შემდეგ კი ითვლება ამ ქულების საშუალო (თითოეულ გუნდს თავისი საშუალო აქვს), გუნდი იგებს მხოლოდ მაშინ, თუ კი მისი საშუალო ორჯერ მაინც იქნება მეტი მეორე გუნდის საშუალოზე, წინააღმდეგ შემთხვევაში, გამარჯვებული არავინ არ არის. თქვენი დავალებაა:
// 1.1. შექმენათ ფუქნცია (calcAverage), რომელიც დაითვლის 3 ქულის საშუალო არითმეტიკულს
// 1.2. გამოიყენათ ფუნქცია თითოეული გუნდის საშუალოს დასათვლელად
// 1.3. შექმენათ ფუნქცია რომელიც შეამოწმებს გამარჯვებულ გუნდს (ex. checkWinner),  მიიღებს ორ პარამეტრს გუნდების საშუალოს სახით და კონსოლში დალოგავს გამარჯვებულ გუნდს თავისი ქულებით (მაგ. პირველმა გუნდმა გაიმარჯვა 31 vs 15 ქულა).
// 1.4 გამოყენეთ ეს ფუნქცია რომ გამოავლინოთ გამარჯვებული გუნდი 1. PowerRangers (44, 23, 71) , FairyTails (65, 54, 49) 2. PowerRangers (85, 54, 41) , FairyTails (23, 34, 47)
// 1.5 optional : ფუნქციაში შეგიძლიათ გაითვალისწინოთ ყაიმის შემთხვევაც.

function calcAverage(a, b, c) {
  return (a + b + c) / 3;
}

// 1
let p1 = calcAverage(44, 23, 71);
let f1 = calcAverage(65, 54, 49);
// 2
let p2 = calcAverage(85, 54, 41);
let f2 = calcAverage(23, 34, 47);

function checkWinner(PowerRangers, FairyTails) {
  if (PowerRangers >= 2 * FairyTails) {
    return console.log(
      `გაიმარჯვა პირველმა გუნდმა ${PowerRangers} vs ${FairyTails}`
    );
  } else if (FairyTails >= 2 * PowerRangers) {
    return console.log(
      `გაიმარჯვა მეორე გუნდმა ${FairyTails} vs ${PowerRangers}`
    );
  } else if (PowerRangers == FairyTails) {
    return console.log("ყაიმი");
  } else return console.log("გამარჯვებული არ გვყავს");
}

console.log(p1);
console.log(f1);
console.log(p2);
console.log(f2);
checkWinner(p1, f1);
checkWinner(p2, f2);

// 2. Tip Calculator

let bills = [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52];

let tipsArray = [];
let finalBillArray = [];
let tip;
let finalBill;

function calcTip(bill) {
  if (bill > 50 && bill < 300) {
    tip = bill * 0.15;
  } else {
    tip = bill * 0.2;
  }
  finalBill = tip + bill;
  tipsArray.push(tip);
  finalBillArray.push(finalBill);
}

for (let i = 0; i < bills.length; i++) {
  calcTip(bills[i]);
}

console.log(tipsArray);
console.log(finalBillArray);

// 2.6. შექმენათ ფუნქცია რომელიც მიიღებს მასივს პარამეტრად და დააბრუნებს საშუალოს,
//  ამ ფუნქციით უნდა დავითვალოთ საშუალოდ რამდენ ჩაის ტოვებს ჩვენი ლუდოვიკო (2.3. პუნტში მიღებული მასივი)
//  და ასევე უნდა დავითვალოთ საშუალოდ რა თანხა დახარჯა მანდ ჭამა-სმაში (2.4. პუნქტის მასივის საშუალო არითმეტიკული)

let sum = 0;

function calcAvg(array) {
  for (let j = 0; j < array.length; j++) {
    sum += array[j];
    avg = sum / array.length;
  }
  return avg;
}

console.log(calcAvg(tipsArray));
console.log(calcAvg(finalBillArray));
