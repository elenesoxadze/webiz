// ამოცანა #1
// Make a program that filters a list of strings and returns a list with only your friends name in it.
// If a name has exactly 4 letters in it, you can be sure that it has to be a friend of yours! Otherwise, you can be sure he's not...
// // Ex: Input = ["Ryan", "Kieran", "Jason", "Yous"], Output = ["Ryan", "Yous"]
// friend ["Ryan", "Kieran", "Mark"] shouldBe ["Ryan", "Mark"]
// ტესტ ქეისები (ამ მასივებზე შეგიძლიათ გატესტოთ ფუნქცია)
// ["George", "Nick", "Tom", "Kate", "Annie"] უნდა დააბრუნოს ["Nick", "Kate"]
// ["James", "Will", "Jack", "Nate", "Edward"] უნდა დააბრუნოს ["Will", "Jack", "Nate"]

let array = ["James", "Will", "Jack", "Nate", "Edward"];
let friendsArray = [];

//-----------version 1------------
function friend(arr, newArr) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].length == 4) {
      result = arr.splice(i, 1, "taken");
      newArr.push(result.toString());
    }
  }
  console.log(newArr);
}

friend(array, friendsArray);

//-----------version 2------------
// function friend(arr, newArr) {
//   for (let i = 0; i < arr.length; i++) {
//     if (arr[i].length == 4) {
//       result = arr.slice(i, i + 1);
//       newArr.push(result.toString());
//     }
//   }
//   console.log(newArr);
// }
// friend(array, friendsArray);

// ამოცანა #2
// Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers.
// No floats or non-positive integers will be passed.
// For example, when an array is passed like [19, 5, 42, 2, 77], the output should be 7.
// [10, 343445353, 3453445, 3453545353453] should return 3453455.

let numbers = [52, 76, 14, 12, 4];

//------------version 1-----------

function lowestPositive(arr) {
  for (let i = 0; i < arr.length; i++) {
    if (arr.length < 4 || arr[i] < 0 || arr[i] % 1 != 0)
      return "you have a negative number or float in your array or maybe there is less than 4 numbers in your array";
  }
  arr.sort((a, b) => {
    return a - b;
  });
  arr.splice(2, arr.length - 1);
  let sum = arr.reduce((a, b) => {
    return a + b;
  }, 0);
  return sum;
}

console.log(lowestPositive(numbers));

//-------------version 2-----------

// function lowestPositive(arr) {
//   for (let i = 0; i < arr.length; i++) {
//     if (arr.length < 4 || arr[i] < 0 || arr[i] % 1 != 0)
//       return "you have a negative number or float in your array or maybe there is less than 4 numbers in your array";
//   }
//   let sum = 0;
//   for (let i = 0; i < arr.length - 2; i++) {
//     let num = Math.min(...arr);
//     sum += num;
//     arr.splice(arr.indexOf(num), 1);
//   }
//   console.log(sum);
// }
// lowestPositive(numbers);
